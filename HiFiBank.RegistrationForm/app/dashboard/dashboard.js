﻿(function () {
    'use strict';
    var controllerId = 'dashboard';
    angular.module('app').controller(controllerId, ['$scope', dashboard]);

    function dashboard($scope) {

        var vm = this;
        vm.title = 'Dashboard2';
        vm.CurrCustomer = {};
        vm.CurrCustomer.Name = "";
        vm.CurrCustomer.Logic = false;
        vm.CurrCustomer.SetThreshold = false;
        vm.CurrCustomer.Threshold;
        vm.saveChanges = function () {
            alert('function called');
            var NewCustomer = vm.CurrCustomer;
            // submit  NewCustomer to DB.
        };
        $scope.toggleSelection = function () {

            vm.CurrCustomer.SetThreshold = !vm.CurrCustomer.SetThreshold;
        };
    }
})();




angular.module('app').directive('bootstrapDropdown', ['$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                require: '?ngModel',
                link: function (scope, element, attrs, ngModel) {

                    $timeout(function () {
                        element.selectpicker().on('change', function () {
                            $(this).validationEngine('validate');
                        });
                        //$('.bootstrap-select').css("width", "120px");
                    });

                    scope.$watch(attrs.ngModel, function (newValue, oldValue) {


                    });
                }
            };
        }
]);
angular.module('app').directive('bootstrapCheck', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        scope: {
            item: '=',
            toggleSelection: '&'
        },
        compile: function (element, $attrs) {
            var icheckOptions = {
                checkboxClass: 'icheckbox_minimal',
                radioClass: 'iradio_minimal'
            };

            var modelAccessor = $parse($attrs['ngModel']);
            return function ($scope, element, $attrs, controller) {
                var modelChanged = function (event) {
                    $scope.toggleSelection({ variable: $scope.item });
                    $scope.$apply();
                };

                $scope.$watch(modelAccessor, function (val) {
                
                    var action = val ? 'check' : 'uncheck';
                    element.iCheck(icheckOptions, action).on('ifChanged', modelChanged);
                });
            };
        }
    };
}]);


angular.module('app').directive('checkValidation', [
        function () {
            return {
                restrict: 'A',
                require: '?ngModel',
                link: function (scope, element, attrs, ngModel) {
                    element.closest('form').validationEngine(
                        {
                            promptPosition: 'centerRight',
                            scroll: true, prettySelect: true,
                            autoPositionUpdate: true,
                            //validateNonVisibleFields: true,
                            //autoHidePrompt: true,
                            autoHideDelay: 1000,
                            fadeDuration: 0.9
                        }
                    );

                    scope.$watch(attrs.ngModel, function (newValue, oldValue) {

                        var valid;
                        if (ngModel.$dirty) {
                            valid = !element.validationEngine('validate'); //check if particular feild is in valid state
                        }
                        else {
                            if (ngModel.$modelValue != null)//when updating and not Dirty
                                valid = !element.validationEngine('validate'); //check if particular feild is in valid state
                            else {//when adding and not Dirty
                                valid = element.validationEngine('validate'); //check if particular feild is in valid state
                            }
                        }
                        ngModel.$setValidity(element.context.name, valid);
                        return valid;
                    });
                }
            };
        }
]);

