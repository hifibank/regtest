﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());
    
    // Configure the routes and route resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);
    function routeConfigurator($routeProvider, routes) {

        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/' });
    }

    // Define the routes 
    function getRoutes() {
    	return [
			    {
			    	url: '/',
			    	config: {
			    		templateUrl: 'app/dashboard/dashboard.html',
			    		title: 'Node List',
			    		settings: {
			    			nav: 1,
			    			content: '<img src="Content/Template/img/shortcuts/nodes.png" alt="">' +
								'<small class="t-overflow">Nodes</small>'
			    		}
			    	}
			    }
        ];
    }
})();


